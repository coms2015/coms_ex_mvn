package study.coms.exchange.stock;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.goods.XGoodsDto;
import study.coms.exchange.store.XStoreDto;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XStockDto implements IValueObject {
    private Integer n;
    private XStoreDto store;
    private Integer amount;
    private Integer inOrder;
    private XGoodsDto goods;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public XStoreDto getStore() {
        return store;
    }

    public void setStore(XStoreDto store) {
        this.store = store;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getInOrder() {
        return inOrder;
    }

    public void setInOrder(Integer inOrder) {
        this.inOrder = inOrder;
    }

    public XGoodsDto getGoods() {
        return goods;
    }

    public void setGoods(XGoodsDto goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XStockDto xStockDto = (XStockDto) o;
        return Objects.equals(n, xStockDto.n) &&
                Objects.equals(store, xStockDto.store) &&
                Objects.equals(amount, xStockDto.amount) &&
                Objects.equals(inOrder, xStockDto.inOrder) &&
                Objects.equals(goods, xStockDto.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, amount, inOrder, goods);
    }
}
