package study.coms.exchange.goods;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.store.XStoreDto;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XGoodsStoreDto implements IValueObject {
    private Integer n;
    private XGoodsDto goods;
    private XStoreDto store;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public XGoodsDto getGoods() {
        return goods;
    }

    public void setGoods(XGoodsDto goods) {
        this.goods = goods;
    }

    public XStoreDto getStore() {
        return store;
    }

    public void setStore(XStoreDto store) {
        this.store = store;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XGoodsStoreDto that = (XGoodsStoreDto) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(goods, that.goods) &&
                Objects.equals(store, that.store);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, goods, store);
    }
}
