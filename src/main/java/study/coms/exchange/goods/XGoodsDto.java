package study.coms.exchange.goods;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XGoodsDto implements IValueObject {
    private Integer n;
    private String code;
    private String name;
    private Collection<XGoodsBarcodeDto> barcodes = new ArrayList<>(0);
    private Collection<XGoodsStoreDto> stores  = new ArrayList<>(0);

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<XGoodsBarcodeDto> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(Collection<XGoodsBarcodeDto> barcodes) {
        this.barcodes = barcodes;
    }

    public Collection<XGoodsStoreDto> getStores() {
        return stores;
    }

    public void setStores(Collection<XGoodsStoreDto> stores) {
        this.stores = stores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XGoodsDto xGoodsDto = (XGoodsDto) o;
        return Objects.equals(n, xGoodsDto.n) &&
                Objects.equals(code, xGoodsDto.code) &&
                Objects.equals(name, xGoodsDto.name) &&
                Objects.equals(barcodes, xGoodsDto.barcodes) &&
                Objects.equals(stores, xGoodsDto.stores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, code, name, barcodes, stores);
    }
}
