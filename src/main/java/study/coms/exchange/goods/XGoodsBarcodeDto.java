package study.coms.exchange.goods;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XGoodsBarcodeDto implements IValueObject {
    private Integer n;
    private String barcode;
    private XGoodsDto goods;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public XGoodsDto getGoods() {
        return goods;
    }

    public void setGoods(XGoodsDto goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XGoodsBarcodeDto that = (XGoodsBarcodeDto) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(barcode, that.barcode) &&
                Objects.equals(goods, that.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, barcode, goods);
    }
}
