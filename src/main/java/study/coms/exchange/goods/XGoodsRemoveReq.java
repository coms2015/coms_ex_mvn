package study.coms.exchange.goods;

@SuppressWarnings("unused")
@Deprecated
public final class XGoodsRemoveReq {
    private Integer goodsN;

    public XGoodsRemoveReq(Integer goodsN) {
        this.goodsN = goodsN;
    }

    public Integer getGoodsN() {
        return goodsN;
    }

    public void setGoodsN(Integer goodsN) {
        this.goodsN = goodsN;
    }
}
