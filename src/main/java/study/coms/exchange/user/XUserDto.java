package study.coms.exchange.user;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XUserDto implements IValueObject {
    private Integer n;
    private String login;
    private String mail;
    private XRoleDto role;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public XRoleDto getRole() {
        return role;
    }

    public void setRole(XRoleDto role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XUserDto xUserDto = (XUserDto) o;
        return Objects.equals(n, xUserDto.n) &&
                Objects.equals(login, xUserDto.login) &&
                Objects.equals(mail, xUserDto.mail) &&
                Objects.equals(role, xUserDto.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, login, mail, role);
    }
}
