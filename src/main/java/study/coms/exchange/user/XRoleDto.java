package study.coms.exchange.user;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XRoleDto implements IValueObject {
    private Integer n;
    private String name;
    private String modules;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModules() {
        return modules;
    }

    public void setModules(String modules) {
        this.modules = modules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XRoleDto xRoleDto = (XRoleDto) o;
        return Objects.equals(n, xRoleDto.n) &&
                Objects.equals(name, xRoleDto.name) &&
                Objects.equals(modules, xRoleDto.modules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, name, modules);
    }
}
