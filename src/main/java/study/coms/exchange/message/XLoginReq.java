package study.coms.exchange.message;

@SuppressWarnings("unused")
public final class XLoginReq {
    private String login;
    private String password;

    public XLoginReq() {
    }

    public XLoginReq(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
