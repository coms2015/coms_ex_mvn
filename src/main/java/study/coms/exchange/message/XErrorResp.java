package study.coms.exchange.message;

@SuppressWarnings("unused")
public final class XErrorResp {
    private Integer errCode;
    private String errMessage;

    public XErrorResp() {
    }

    public XErrorResp(Integer errCode, String errMessage) {
        this.errCode = errCode;
        this.errMessage = errMessage;
    }

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
}
