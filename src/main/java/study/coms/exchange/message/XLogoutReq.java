package study.coms.exchange.message;

@SuppressWarnings("unused")
public final class XLogoutReq {
    private Integer logout = null;

    public Integer getLogout() {
        return logout;
    }

    public void setLogout(Integer logout) {
        this.logout = logout;
    }
}
