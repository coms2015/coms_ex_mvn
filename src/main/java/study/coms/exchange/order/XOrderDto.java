package study.coms.exchange.order;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.store.XStoreDto;
import study.coms.exchange.user.XUserDto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XOrderDto implements IValueObject {
    private Integer n;
    private XStoreDto store;
    private String code;
    private XUserDto user;
    private Date createdAt;
    private Date executedAt;
    private Integer executed;
    private Collection<XOrderGoodsDto> goods = new ArrayList<>(0);

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public XStoreDto getStore() {
        return store;
    }

    public void setStore(XStoreDto store) {
        this.store = store;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public XUserDto getUser() {
        return user;
    }

    public void setUser(XUserDto user) {
        this.user = user;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getExecutedAt() {
        return executedAt;
    }

    public void setExecutedAt(Date executedAt) {
        this.executedAt = executedAt;
    }

    public Integer getExecuted() {
        return executed;
    }

    public void setExecuted(Integer executed) {
        this.executed = executed;
    }

    public Collection<XOrderGoodsDto> getGoods() {
        return goods;
    }

    public void setGoods(Collection<XOrderGoodsDto> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XOrderDto xOrderDto = (XOrderDto) o;
        return Objects.equals(n, xOrderDto.n) &&
                Objects.equals(store, xOrderDto.store) &&
                Objects.equals(code, xOrderDto.code) &&
                Objects.equals(user, xOrderDto.user) &&
                Objects.equals(createdAt, xOrderDto.createdAt) &&
                Objects.equals(executedAt, xOrderDto.executedAt) &&
                Objects.equals(executed, xOrderDto.executed) &&
                Objects.equals(goods, xOrderDto.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, code, user, createdAt, executedAt, executed, goods);
    }
}
