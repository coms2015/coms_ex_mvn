package study.coms.exchange.order;

@SuppressWarnings("unused")
@Deprecated
public final class XOrderRemoveReq {
    private Integer orderRemoveN;

    public XOrderRemoveReq(Integer orderRemoveN) {
        this.orderRemoveN = orderRemoveN;
    }

    public Integer getOrderRemoveN() {
        return orderRemoveN;
    }

    public void setOrderRemoveN(Integer orderRemoveN) {
        this.orderRemoveN = orderRemoveN;
    }
}
