package study.coms.exchange.order;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.goods.XGoodsDto;
import study.coms.exchange.stock.XStockDto;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")

public final class XOrderGoodsDto implements IValueObject {
    private Integer n;
    private XOrderDto order;
    private XGoodsDto goods;
    private Integer amount;
    private XStockDto stock;


    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public XOrderDto getOrder() {
        return order;
    }

    public void setOrder(XOrderDto order) {
        this.order = order;
    }

    public XGoodsDto getGoods() {
        return goods;
    }

    public void setGoods(XGoodsDto goods) {
        this.goods = goods;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public XStockDto getStock() {
        return stock;
    }

    public void setStock(XStockDto stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XOrderGoodsDto that = (XOrderGoodsDto) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(order, that.order) &&
                Objects.equals(goods, that.goods) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(stock, that.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, order, goods, amount, stock);
    }
}
