package study.coms.exchange.order;

@SuppressWarnings("unused")
@Deprecated
public final class XOrderOutReq {
    private Integer orderOutN;

    public XOrderOutReq(Integer orderOutN) {
        this.orderOutN = orderOutN;
    }

    public Integer getOrderOutN() {
        return orderOutN;
    }

    public void setOrderOutN(Integer orderOutN) {
        this.orderOutN = orderOutN;
    }
}
