package study.coms.exchange.order;

@SuppressWarnings("unused")
@Deprecated
public final class XOrderUnreserveReq {
    private Integer orderUnreserveN;

    public XOrderUnreserveReq(Integer orderUnreserveN) {
        this.orderUnreserveN = orderUnreserveN;
    }

    public Integer getOrderUnreserveN() {
        return orderUnreserveN;
    }

    public void setOrderUnreserveN(Integer orderUnreserveN) {
        this.orderUnreserveN = orderUnreserveN;
    }
}
