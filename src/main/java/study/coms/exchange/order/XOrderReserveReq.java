package study.coms.exchange.order;

@SuppressWarnings("unused")
@Deprecated
public final class XOrderReserveReq {
    public Integer orderReserveN;

    public XOrderReserveReq(Integer orderReserveN) {
        this.orderReserveN = orderReserveN;
    }

    public Integer getOrderReserveN() {
        return orderReserveN;
    }

    public void setOrderReserveN(Integer orderReserveN) {
        this.orderReserveN = orderReserveN;
    }
}
