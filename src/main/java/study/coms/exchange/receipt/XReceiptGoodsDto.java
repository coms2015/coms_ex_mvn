package study.coms.exchange.receipt;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.goods.XGoodsDto;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XReceiptGoodsDto implements IValueObject {
    private Integer n;
    private Integer amount;
    private XReceiptDto receipt;
    private XGoodsDto goods;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public XReceiptDto getReceipt() {
        return receipt;
    }

    public void setReceipt(XReceiptDto receipt) {
        this.receipt = receipt;
    }

    public XGoodsDto getGoods() {
        return goods;
    }

    public void setGoods(XGoodsDto goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XReceiptGoodsDto that = (XReceiptGoodsDto) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(receipt, that.receipt) &&
                Objects.equals(goods, that.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, amount, receipt, goods);
    }
}
