package study.coms.exchange.receipt;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;
import study.coms.exchange.store.XStoreDto;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XReceiptDto implements IValueObject {
    private Integer n;
    private XStoreDto store;
    private String code;
    private Date createdAt;
    private Date arrivalAt;
    private Integer arrived;
    private Collection<XReceiptGoodsDto> goods = new ArrayList<>(0);

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public XStoreDto getStore() {
        return store;
    }

    public void setStore(XStoreDto store) {
        this.store = store;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getArrivalAt() {
        return arrivalAt;
    }

    public void setArrivalAt(Date arrivalAt) {
        this.arrivalAt = arrivalAt;
    }

    public Integer getArrived() {
        return arrived;
    }

    public void setArrived(Integer arrived) {
        this.arrived = arrived;
    }

    public Collection<XReceiptGoodsDto> getGoods() {
        return goods;
    }

    public void setGoods(Collection<XReceiptGoodsDto> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XReceiptDto that = (XReceiptDto) o;
        return Objects.equals(n, that.n) &&
                Objects.equals(store, that.store) &&
                Objects.equals(code, that.code) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(arrivalAt, that.arrivalAt) &&
                Objects.equals(arrived, that.arrived) &&
                Objects.equals(goods, that.goods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, store, code, createdAt, arrivalAt, arrived, goods);
    }
}
