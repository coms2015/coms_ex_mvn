package study.coms.exchange.receipt;

@SuppressWarnings("unused")
@Deprecated
public final class XReceiptInReq {
    private Integer receiptInN;

    public XReceiptInReq(Integer receiptInN) {
        this.receiptInN = receiptInN;
    }

    public Integer getReceiptInN() {
        return receiptInN;
    }

    public void setReceiptInN(Integer receiptInN) {
        this.receiptInN = receiptInN;
    }
}
