package study.coms.exchange.receipt;

@SuppressWarnings("unused")
@Deprecated
public final class XReceiptRemoveReq {
    private Integer receiptRemoveN;

    public XReceiptRemoveReq(Integer receiptRemoveN) {
        this.receiptRemoveN = receiptRemoveN;
    }

    public Integer getReceiptRemoveN() {
        return receiptRemoveN;
    }

    public void setReceiptRemoveN(Integer receiptRemoveN) {
        this.receiptRemoveN = receiptRemoveN;
    }
}
