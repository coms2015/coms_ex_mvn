package study.coms.exchange.store;

@SuppressWarnings("unused")
@Deprecated
public final class XStoreRemoveReq {
    private Integer storeN;

    public XStoreRemoveReq(Integer storeN) {
        this.storeN = storeN;
    }

    public Integer getStoreN() {
        return storeN;
    }

    public void setStoreN(Integer storeN) {
        this.storeN = storeN;
    }
}
