package study.coms.exchange.store;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import study.coms.exchange.IValueObject;

import java.util.Objects;

@SuppressWarnings("unused")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "n")
public final class XStoreDto implements IValueObject {
    private Integer n;
    private String name;
    private String address;

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XStoreDto xStoreDto = (XStoreDto) o;
        return Objects.equals(n, xStoreDto.n) &&
                Objects.equals(name, xStoreDto.name) &&
                Objects.equals(address, xStoreDto.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(n, name, address);
    }
}
